package ticketsapp.controller;

import ticketsapp.domain.Account;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class AccountController {

@RequestMapping (value = "/account/registration", method = RequestMethod.GET)
    public String registerNewUser(Model model){
    Account account = new Account();
    model.addAttribute("newUser", account);
    return "registration";
}

}
