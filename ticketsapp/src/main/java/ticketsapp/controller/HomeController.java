package ticketsapp.controller;

import ticketsapp.account.AccountService;
import ticketsapp.domain.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class HomeController {

    private final AccountService accountService;

    @Autowired
    public HomeController(AccountService accountService) {
        this.accountService = accountService;
    }

    @RequestMapping("/")
    public String welcome (Model model){
        model.addAttribute("greeting", "Welcome to best tickets-booking webservice!");
        model.addAttribute("tagline", "It's best, I swear");
        return "index";
    }

    @RequestMapping(value = "account/save", method = RequestMethod.POST)
    public String saveAccount(@ModelAttribute("newUser") Account account, BindingResult result) {
//        accountService.create(account);

        return "redirect:/";
    }
}
